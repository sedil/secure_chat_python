import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit, QFileDialog, QPushButton, QVBoxLayout, QRadioButton, QGridLayout, QMenuBar, QMenu, QAction
import threading

sys.path.append('../')
from root.Server import Server
from root.Client import Client
from root.Security import KeyGen
from root.Manager import Manager

from gui.Serverwindow import Serverwindow
from gui.Clientwindow import Clientwindow

# Speicherzugriffsfehler Clientseitig?
# Port bei beenden freigeben

class Mainwindow(QWidget):

    def __init__(self, parent=None):
        super(Mainwindow, self).__init__(parent)
        self.serverFlag = False
        self.key = None

        self.initCompnents()
        self.initLayout()
        self.setLayout(self.layout)
        self.setWindowTitle('Configure')
        self.show()

    def initCompnents(self):
        self.menubar = QMenuBar()
        self.menu = QMenu('Options')
        self.menu_createKey = QAction('Create and save a key')
        self.menu_loadKey = QAction('Load a key')

        self.menu_createKey.triggered.connect(self.menuChooser)
        self.menu_loadKey.triggered.connect(self.menuChooser)

        self.menu.addAction(self.menu_createKey)
        self.menu.addAction(self.menu_loadKey)
        self.menubar.addMenu(self.menu)

        self.ipLabel = QLabel('IP')
        self.portLabel = QLabel('Port')
        self.nameLabel = QLabel('Name')
        self.isServer = QRadioButton('Server')
        self.isServer.clicked.connect(self.checkRadioButton)

        self.createChat = QPushButton('Start')
        self.createChat.clicked.connect(self.examineEntries)

        self.ip = QLineEdit()
        self.port = QLineEdit()
        self.name = QLineEdit()

    def initLayout(self):
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.menubar)

        self.gridlayout = QGridLayout()
        self.gridlayout.addWidget(self.ipLabel, 0, 0)
        self.gridlayout.addWidget(self.ip, 0, 1)
        self.gridlayout.addWidget(self.portLabel, 1 ,0)
        self.gridlayout.addWidget(self.port, 1, 1)
        self.gridlayout.addWidget(self.nameLabel, 2, 0)
        self.gridlayout.addWidget(self.name, 2, 1)
        self.gridlayout.addWidget(self.isServer, 3, 0)
        self.gridlayout.addWidget(self.createChat, 3, 1)

        self.layout.addLayout(self.gridlayout)

    def menuChooser(self):
        action = self.sender().text()
        if action == 'Create and save a key':
            keygen = KeyGen(128)
            key = keygen.generateKey()
            file = QFileDialog.getSaveFileName(self,'Save key','/home/')
            manager = Manager()
            manager.saveKey(file[0], key)
        elif action == 'Load a key':
            file = QFileDialog.getOpenFileName(self,'Load key','/home/','key file (*.pk)')
            manager = Manager()
            self.key = manager.loadKey(file[0])

    def checkRadioButton(self):
        if self.isServer.isChecked():
            self.serverFlag = True
        else:
            self.serverFlag = False

    def examineEntries(self):
        '''
        if not self.key:
            print('Please load a key first')
            return False
        ip = self.ip.text()
        try:
            port = int(self.port.text())
            if port < 1024 or port > 65535:
                print('Port must be a number between 1024 and 65535')
                return False
        except ValueError:
            print('Port must be a number between 1024 and 65535')
            return False
        name = self.name.text()
        if len(name) == 0 or len(name) > 10:
            print('Name is empty or greater than 10 characters')
            return False
        '''

        ip = '127.0.0.1'
        port = 8080
        name = "Frank"
        manager = Manager()
        self.key = manager.loadKey('/home/sebastian/Schreibtisch/chat.pk')


        if self.serverFlag:
            servergui = Serverwindow(self)

            server = Server(ip, port, name, self.key, servergui)
            self.background_thread = threading.Thread(target=server.handleClient)
            self.background_thread.daemon = True
            self.background_thread.start()
            servergui.connectServer(server)
            self.hide()
        else:
            clientgui = Clientwindow(self)

            client = Client(ip, port, name, self.key, clientgui)
            if not client.connectToServer():
                sys.exit()
            self.background_thread = threading.Thread(target=client.receive_message)
            self.background_thread.daemon = True
            self.background_thread.start()
            clientgui.connectClient(client)
            self.hide()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Mainwindow()
    sys.exit(app.exec_())