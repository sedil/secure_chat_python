from PyQt5.QtWidgets import QWidget, QDialog, QVBoxLayout, QHBoxLayout, QTextEdit, QPushButton, QMenuBar, QMenu, QAction

class Clientwindow(QDialog):

    def __init__(self, parent=None):
        super(Clientwindow, self).__init__(parent)
        self.initComponents()
        self.initLayout()
        self.setLayout(self.layout)
        self.setWindowTitle('Chat - Client')
        self.show()

    def connectClient(self, client):
        self.client = client

    def initComponents(self):
        self.menubar = QMenuBar()
        self.menu = QMenu('Options')
        self.menu_exit = QAction('Exit')
        self.menu_exit.setShortcut('Ctrl+X')
        self.menu_exit.triggered.connect(self.menuChooser)

        self.menu.addAction(self.menu_exit)
        self.menubar.addMenu(self.menu)

        self.chattext = QTextEdit()
        self.chattext.setFixedSize(720, 480)
        self.chattext.setReadOnly(True)

        self.inputtext = QTextEdit()
        self.inputtext.setFixedSize(620, 100)
        self.sendButton = QPushButton('send')
        self.sendButton.setFixedSize(80, 80)
        self.sendButton.clicked.connect(self.sendToClient)

    def initLayout(self):
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.menubar)
        self.layout.addWidget(self.chattext)

        self.chatentry = QHBoxLayout()
        self.chatentry.addWidget(self.inputtext)
        self.chatentry.addWidget(self.sendButton)

        self.layout.addLayout(self.chatentry)

    def sendToClient(self):
        entry = self.inputtext.toPlainText()
        if len(entry) == 0:
            return

        self.client.send_message(entry)

        self.chattext.append(entry)
        self.inputtext.clear()

    def receiveMessage(self, text):
        self.chattext.append(text)

    def menuChooser(self):
        action = self.sender().text()
        if action == 'Exit':
            self.close()