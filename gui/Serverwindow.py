from PyQt5.QtWidgets import QWidget, QDialog, QVBoxLayout, QHBoxLayout, QTextEdit, QPushButton, QMenuBar, QMenu, QAction
import threading

class Serverwindow(QDialog):

    def __init__(self, parent=None):
        super(Serverwindow, self).__init__(parent)
        self.initComponents()
        self.initLayout()
        self.setLayout(self.layout)
        self.setWindowTitle('Chat - Server')
        self.show()

    def connectServer(self, server):
        self.server = server

    def initComponents(self):
        self.menubar = QMenuBar()
        self.menu = QMenu('Options')
        self.menu_exit = QAction('Exit')
        self.menu_exit.setShortcut('Ctrl+X')
        self.menu_exit.triggered.connect(self.menuChooser)

        self.menu.addAction(self.menu_exit)
        self.menubar.addMenu(self.menu)

        self.chattext = QTextEdit()
        self.chattext.setFixedSize(720, 480)
        self.chattext.setReadOnly(True)

        self.inputtext = QTextEdit()
        self.inputtext.setFixedSize(620, 100)
        self.sendButton = QPushButton('send')
        self.sendButton.setFixedSize(80, 80)
        self.sendButton.clicked.connect(self.sendToServer)

    def initLayout(self):
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.menubar)
        self.layout.addWidget(self.chattext)

        self.chatentry = QHBoxLayout()
        self.chatentry.addWidget(self.inputtext)
        self.chatentry.addWidget(self.sendButton)

        self.layout.addLayout(self.chatentry)

    def sendToServer(self):
        entry = self.inputtext.toPlainText()
        if len(entry) == 0:
            return

        self.server.send_message(entry)

        self.chattext.append(entry)
        self.inputtext.clear()

    def receiveMessage(self, text):
        self.chattext.append(text)

    def menuChooser(self):
        action = self.sender().text()
        if action == 'Exit':
            self.close()