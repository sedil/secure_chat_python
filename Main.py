import sys
from root.Security import KeyGen
from root.Manager import Manager
from root.Server import Server
from root.Client import Client

# dynamische Wahl von IP, Host, Pfad und Keylength
# einfache GUI

if __name__ == "__main__":

    # k = Schlüssel generieren und speichern
    # s = Server + Schlüssel laden
    # c = Client + Schlüssel laden

    if sys.argv[1] == 's':
        manager = Manager()
        key = manager.loadKey('/home/sebastian/Schreibtisch/otpkey.pk')
        if not key:
            print('no key found')
        else:
            server = Server('127.0.0.1',8080, 'Server', key)
            server.handleClient()
    elif sys.argv[1] == 'c':
        manager = Manager()
        key = manager.loadKey('/home/sebastian/Schreibtisch/otpkey.pk')
        if not key:
            print('no key found')
        else:
            client = Client('127.0.0.1',8080, 'Client', key)
            if not client.connectToServer():
                sys.exit()
            client.sendMessage()
    elif sys.argv[1] == 'k':
        keygen = KeyGen(128)
        key = keygen.generateKey()
        manager = Manager()
        manager.saveKey('/home/sebastian/Schreibtisch/', key)



    '''
    otp = OTP()

    if sys.argv[1] == '1':
        keygen = KeyGen(128)
        key = keygen.generateKey()
        manager = Manager()
        manager.saveKey('/home/sebastian/Schreibtisch/',key)
    elif sys.argv[1] == '2':
        manager = Manager()
        key = manager.loadKey('/home/sebastian/Schreibtisch/otpkey.pk')

        text = "Ein Text der verschlüsselt wird."
        print(f'Klar={text}')
        cipher = otp.encrypth_text(text, key)
        print(f'Cipher={cipher}')
        # übertragung
        klar = otp.decrypth_text(cipher, key)
        print(f'Klar={klar}')
    else:
        print('wrong usage')
    '''