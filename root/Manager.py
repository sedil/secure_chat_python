import os

class Manager:

    def __init__(self):
        pass

    def saveKey(self, path, key):
        file = open(path + '.pk','w')
        file.write(key)
        file.close()

    def loadKey(self, path):
        extension = path.split('.')
        if len(extension) < 2 or extension[1] != 'pk':
            print('unsupported extension')
            return None

        try:
            file = open(path,'r')
            key = file.read()
            file.close()
            return key
        except FileNotFoundError as e:
            print(e)
            return None