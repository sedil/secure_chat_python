import math, random


class OTP:

    def __init__(self):
        pass

    def encrypth_text(self, plaintext, key):
        assert(len(plaintext) <= len(key))
        m = plaintext.encode('utf-8')
        k = key.encode()
        l = []
        for i, elem in enumerate(m):
            l.append(elem ^ k[i])
        return bytes(l)

    def decrypth_text(self, ciphertext, key):
        k = key.encode()
        l = []
        for i, elem in enumerate(ciphertext):
            l.append(elem ^ k[i])
        return bytes(l).decode('utf-8')


class KeyGen:

    def __init__(self, length):
        self.keylength = length

    def generateKey(self):
        available = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ°!§$%&/()=?+*~#,;.:-_<>|'
        length = len(available)
        key = ''
        for i in range(self.keylength):
            key += available[math.floor(random.random() * length)]
        return key
