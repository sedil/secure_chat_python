import socket
import threading
import datetime
from root.Security import OTP


class Server:

    def __init__(self, ip, port, name, key, gui):
        self.key = key
        self.name = name
        self.gui = gui
        self.otp = OTP()

        self.address = (ip, port)
        self.disconnect = '!EXIT'
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(self.address)
        self.gui.receiveMessage(f'Servers IP is :{ip}')

    def send_message(self, message):
        if len(message) == 0 or message=='-':
            print('empty message from server')
        else:
            text = f'{self.name} -> {message}'
            cipher = self.otp.encrypth_text(text, self.key)

            try:
                self.conn[0].send(cipher)
            except BrokenPipeError:
                self.gui.receiveMessage('Could not send data to Client.')
                self.server.close()

    def handleClient(self):
        self.server.listen(1)
        self.gui.receiveMessage(f'Server is listening on {self.address}')
        self.conn = self.server.accept()

        #self.background_thread = threading.Thread(target=self.sendMessage, args="-")
        #self.background_thread.daemon = True
        #self.background_thread.start()

        self.gui.receiveMessage(f'Connected with {self.conn[1]}')
        self.clientname = self.otp.decrypth_text(self.conn[0].recv(64), self.key)
        self.gui.receiveMessage(f'{self.clientname} has joined the chat')

        while True:
            temp = self.conn[0].recv(2048)
            if not len(temp):
                break
            else:
                temp = self.otp.decrypth_text(temp, self.key)
                time = '[' + str(datetime.datetime.now().date()) + ' ' + str(datetime.datetime.now().hour) + ':' + str(
                        datetime.datetime.now().minute) + ':' + str(datetime.datetime.now().second) + ']'
                self.gui.receiveMessage(f'{time} {temp}')

        '''
        try:
            for message in iter(lambda: self.otp.decrypth_text(self.conn[0].recv(2048), self.key), ''):
                if message == self.disconnect:
                    self.server.close()
                    self.gui.receiveMessage('Connection closed')
                else:
                    time = '[' + str(datetime.datetime.now().date()) + ' ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute) + ':' + str(datetime.datetime.now().second) + ']'
                    self.gui.receiveMessage(f'{time} {message}')
        except BrokenPipeError:
            print('s1')
        except ConnectionRefusedError:
            print('s2')
        except ConnectionAbortedError:
            print('s3')
        except ConnectionResetError:
            print('s4')
        except ConnectionError:
            print('s5')
        except StopIteration:
            print('s6')
        except Exception:
            print('s7')
        '''