import socket
import threading
import datetime
from root.Security import OTP


class Client:

    def __init__(self, ip ,port, name, key, gui):
        self.address = (ip, port)
        self.key = key
        self.name = name
        self.gui = gui
        self.otp = OTP()
        self.disconnect = '!EXIT'
        self.client = socket.socket()

    def connectToServer(self):
        cipher = self.otp.encrypth_text(self.name, self.key)

        try:
            self.client.connect(self.address)
            self.client.send(cipher)
            self.gui.receiveMessage(f'Connected with {self.address}')
            #self.background_thread = threading.Thread(target=self.receiveMessage)
            #self.background_thread.daemon = True
            #self.background_thread.start()
            return True
        except BrokenPipeError:
            self.gui.receiveMessage('Could not send inital data to server')
            return False
        except ConnectionRefusedError:
            self.gui.receiveMessage('Could not connect to server')
            return False

    def receive_message(self):
        while True:
            temp = self.client.recv(2048)
            if not len(temp):
                break
            else:
                temp = self.otp.decrypth_text(temp, self.key)
                time = '[' + str(datetime.datetime.now().date()) + ' ' + str(datetime.datetime.now().hour) + ':' + str(
                        datetime.datetime.now().minute) + ':' + str(datetime.datetime.now().second) + ']'
                self.gui.receiveMessage(f'{time} {temp}')

        '''
        try:
            for message in iter(lambda: self.otp.decrypth_text(self.client.recv(2048), self.key), ''):
                if message == self.disconnect:
                    self.client.close()
                    self.gui.receiveMessage('Connection closed')
                else:
                    time = '[' + str(datetime.datetime.now().date()) + ' ' + str(datetime.datetime.now().hour) + ':' + str(datetime.datetime.now().minute) + ':' + str(datetime.datetime.now().second) + ']'
                    self.gui.receiveMessage(f'{time} {message}')
        except BrokenPipeError:
            print('c1')
        except ConnectionRefusedError:
            print('c2')
        except ConnectionAbortedError:
            print('c3')
        except ConnectionResetError:
            print('c4')
        except ConnectionError:
            print('c5')
        except StopIteration:
            print('c6')
        except Exception:
            print('c7')
        '''

    def send_message(self, message):
        if len(message) == 0:
            print('empty message from client')
        else:
            text = f'{self.name} -> {message}'
            cipher = self.otp.encrypth_text(text, self.key)

            try:
                self.client.send(cipher)
            except BrokenPipeError:
                self.gui.receiveMessage('Could not sent data to server. Exit now.')
                self.client.close()